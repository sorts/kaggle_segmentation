import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm

import params

input_size = params.input_size
batch_size = params.batch_size
orig_width = params.orig_width
orig_height = params.orig_height
threshold = params.threshold
model = params.model
BASE_DIR = params.BASE_DIR

df_test = pd.read_csv('input/list_view_test.csv')
ids_test = df_test['img'].map(lambda s: s.split('.')[0])

names = []
for id in ids_test:
    for it in range(1,17):
        names.append('{}{num:02d}.jpg'.format(id,num=it))


# https://www.kaggle.com/stainsby/fast-tested-rle
def run_length_encode(mask):
    '''
    img: numpy array, 1 - mask, 0 - background
    Returns run length as string formated
    '''
    inds = mask.flatten()
    runs = np.where(inds[1:] != inds[:-1])[0] + 2
    runs[1::2] = runs[1::2] - runs[:-1:2]
    rle = ' '.join([str(r) for r in runs])
    return rle


rles = []
ordered_rles = []

for i in range(1,17):

    # aux_rles = []
    # model.load_weights(filepath='weights/best_weights_viewn{num:02d}.hdf5'.format(num=i))
    #
    # print('Predicting on {} samples with batch_size = {}... model no: {}'.format(len(ids_test), batch_size, i))
    # for start in tqdm(range(0, len(ids_test), batch_size)):
    #     x_batch = []
    #     end = min(start + batch_size, len(ids_test))
    #     ids_test_batch = ids_test[start:end]
    #     for id in ids_test_batch.values:
    #         img = cv2.imread('{}/test/{}{num:02d}.jpg'.format(BASE_DIR,id,num=i))
    #         #img = cv2.resize(img, (input_size, input_size))
    #         img1 = img[0:1280, 0:1280]
    #         img2 = img[0:1280, 638:1918]
    #         x_batch.append(img1)
    #         x_batch.append(img2)
    #
    #     x_batch = np.array(x_batch, np.float32) / 255
    #     preds = model.predict_on_batch(x_batch)
    #     preds = np.squeeze(preds, axis=3)
    #     prob = np.zeros([orig_height, orig_width])
    #
    #     slice_counter = 0
    #     for pred in preds:
    #
    #         if(slice_counter==0):
    #             prob[0:1280,0:1280] = pred
    #         else:
    #             prob[0:1280, 638:1918] = pred
    #         slice_counter+=1
    #         #prob = cv2.resize(pred, (orig_width, orig_height))
    #
    #     #plt.imshow(prob)
    #     #plt.show()
    #     mask = prob > threshold
    #     rle = run_length_encode(mask)
    #     aux_rles.append(rle)

    #np.save('rles_view{num:02d}.npy'.format(num=i), aux_rles)
    aux_rles = np.load('rles_view{num:02d}.npy'.format(num=i))
    ordered_rles.append(aux_rles)

# traverse ordered aux RLES
for j in range(len(ids_test)):
    for ii in range(0,16):
        rles.append( ordered_rles[ii][j] )

print("Generating submission file...")
df = pd.DataFrame({'img': names, 'rle_mask': rles})
df.to_csv('submit/submission.csv.gz', index=False, compression='gzip')
