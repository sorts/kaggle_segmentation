import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm

from skimage.measure import label
from skimage import measure
from skimage import data, util
try:
    from skimage import filters
except ImportError:
    from skimage import filter as filters

import params
import sys
from numpy import inf

# Get im{read,write} from somewhere.
try:
    from cv2 import imread, imwrite
except ImportError:
    # Note that, sadly, skimage unconditionally import scipy and matplotlib,
    # so you'll need them if you don't have OpenCV. But you probably have them.
    from skimage.io import imread, imsave
    imwrite = imsave
    # TODO: Use scipy instead.

input_size = params.input_size
batch_size = params.batch_size
orig_width = params.orig_width
orig_height = params.orig_height
threshold = params.threshold
model = params.model

# draw -----------------------------------
def im_show(name, image, resize=1):
    H,W = image.shape[0:2]
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.imshow(name, image.astype(np.uint8))
    cv2.resizeWindow(name, round(resize*W), round(resize*H))


def draw_shadow_text(img, text, pt,  fontScale, color, thickness, color1=None, thickness1=None):
    if color1 is None: color1=(0,0,0)
    if thickness1 is None: thickness1 = thickness+2

    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(img, text, pt, font, fontScale, color1, thickness1, cv2.LINE_AA)
    cv2.putText(img, text, pt, font, fontScale, color,  thickness,  cv2.LINE_AA)

def draw_contour(image, mask, color=(0,255,0), thickness=1):
    threshold = 127
    ret, thresh = cv2.threshold(mask,threshold,255,0)
    ret = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    hierarchy = ret[0]
    contours  = ret[1]
    #image[...]=image
    cv2.drawContours(image, contours, -1, color, thickness, cv2.LINE_AA)
    ## drawContours(image, contours, contourIdx, color, thickness=None, lineType=None, hierarchy=None, maxLevel=None, offset=None): # real signature unknown; restored from __doc__


def draw_mask(img,  mask,  color=(0,255,0), alpha=1., beta=0.8):
    mask = np.dstack((mask,mask,mask))*np.array(color)
    mask = mask.astype(np.uint8)
    img[...] = cv2.addWeighted(img,  alpha, mask, beta,  0.) # image * α + mask * β + λ


#debug and show -----------------------------------------------------------------
def draw_contour_on_image(image, label, prob=None):
    results = image.copy()
    #if prob is not None:
    #    draw_contour(results, prob, color=(0,255,0), thickness=1)
    if label is not None:
        draw_contour(results, label, color=(0,0,255), thickness=1)

    return results

def draw_dice_on_image(image, label, prob, name=''):

    label = label>127
    prob = prob > 0.5

    score = one_dice_loss_py(label, prob)

    H,W,C = image.shape
    results = np.zeros((H*W,3),np.uint8)
    a = (2*label+prob).reshape(-1)
    miss = np.where(a==2)[0]
    hit  = np.where(a==3)[0]
    fp   = np.where(a==1)[0]
    label_sum = label.sum()
    prob_sum  = prob.sum()

    results[miss] = np.array([0,0,255])
    results[hit]  = np.array([64,64,64])
    results[fp]   = np.array([0,255,0])
    results = results.reshape(H,W,3)
    L=30
    draw_shadow_text  (results, '%s'%(name), (5,1*L),  1, (255,255,255), 2)
    draw_shadow_text  (results, '%0.5f'%(score), (5,2*L),  1, (255,255,255), 2)
    draw_shadow_text  (results, 'label = %0.0f'%(label_sum), (5,3*L),  1, (255,255,255), 2)
    draw_shadow_text  (results, 'prob = %0.0f (%0.4f)'%(prob_sum,prob_sum/label_sum), (5,4*L),  1, (255,255,255), 2)
    draw_shadow_text  (results, 'miss = %0.0f (%0.4f)'%(len(miss), len(miss)/label_sum), (5,5*L),  1, (0,0,255), 2)
    draw_shadow_text  (results, 'fp   = %0.0f (%0.4f)'%(len(fp), len(fp)/prob_sum), (5,6*L),  1, (0,255,0), 2)

    return results,score


def one_dice_loss_py(m1, m2):
    m1 = m1.reshape(-1)
    m2 = m2.reshape(-1)
    intersection = (m1 * m2)
    score = 2. * (intersection.sum()+1) / (m1.sum() + m2.sum()+1)
    return score

def remove_small_connected_components(mask, min_size):

    # find connected components
    blobs_labels, num_ccs = measure.label(mask, neighbors=8, background=2, return_num=True)
    #print("num ccs {}".format(num_ccs))
    props = measure.regionprops(blobs_labels)

    for i in range(0,num_ccs):
        if( props[i].area < min_size):
            cc_cords = props[i].coords
            for j in range(0,len(cc_cords)):
                mask[cc_cords[j][0], cc_cords[j][1]] = not mask[cc_cords[j][0], cc_cords[j][1]]

    return mask

df_test = pd.read_csv('input/validation.csv')
ids_test = df_test['img'].map(lambda s: s.split('.')[0])

names = []
for id in ids_test:
    names.append('{}.jpg'.format(id))

model.load_weights(filepath='weights/best_weights_1280_sliding_window.hdf5')

filenames = []
dice_scores = []

index = 0

print('Predicting on {} samples with batch_size = {}...'.format(len(ids_test), batch_size))
for start in tqdm(range(0, len(ids_test), batch_size)):
    end = min(start + batch_size, len(ids_test))
    ids_test_batch = ids_test[start:end]

    x_batch = []
    for id in ids_test_batch.values:
        print('input/train/{}.jpg'.format(id))
        filenames.append(format(id))
        img = cv2.imread('input/train/{}.jpg'.format(id))

        img1 = img[0:1280, 0:1280]
        img2 = img[0:1280, 638:1918]
        x_batch.append(img1)
        x_batch.append(img2)

        #img = cv2.resize(img, (input_size, input_size))
        #x_batch.append(img)
    x_batch = np.array(x_batch, np.float32) / 255
    preds = model.predict_on_batch(x_batch)
    preds = np.squeeze(preds, axis=3)

    prob = np.zeros([orig_height, orig_width])
    slice_counter = 0

    for pred in preds:
        #prob = cv2.resize(pred, (orig_width, orig_height))

        if (slice_counter == 0):
            prob[0:1280, 0:1280] = pred
        else:
            prob[0:1280, 638:1918] = pred
        slice_counter += 1

    #prob = cv2.medianBlur(prob, 5)
    mask = prob > threshold

    # CC removal
    mask = remove_small_connected_components(mask, 50)
    #plt.imshow(mask)
    #plt.show()

    str_img = 'input\\train\\%s.jpg'%filenames[index]
    str_label = 'input\\train_masks\\%s_mask.png'%filenames[index]
    str_dice = 'input\\dice\\%s_dice.png'%filenames[index]

    img = cv2.imread(str_img)
    label = cv2.imread(str_label, cv2.IMREAD_GRAYSCALE)

    res, aux_score = draw_dice_on_image(img, label, prob, filenames[index])
    dice_scores.append(aux_score) # save score

    cv2.imwrite(str_dice, res)

    #im_show('dice', res, resize=0.33)
    #cv2.waitKey(0)

    #filename_prob = '%s.npy'%filenames[index]
    #np.save(filename_prob, prob)
    index+=1

    #plt.imshow(prob)
    #plt.show()

    #mask = mask.astype(int)
    #mask = mask*255
    #cv2.imwrite("test.png",mask)

sorted_lists = sorted(zip(filenames, dice_scores), reverse=True, key=lambda x: x[1])
filenames, dice_scores = [[x[i] for x in sorted_lists] for i in range(2)]

index = 0
for id in filenames:
    print('input/train/{}.jpg score: {}'.format(id,dice_scores[index]))
    index+=1

print('mean error {}' .format(np.average(np.array(dice_scores))))