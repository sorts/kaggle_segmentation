import numpy as np
import cv2
import glob
import sys
import matplotlib.pyplot as plt
import math

class JointBilateralFiltering:
    def __init__(self, color_img, prob_img, r):
        self.color_img_ = color_img
        self.prob_img_ = prob_img
        self.R_ = r # windows radius
        self.D_ = self.R_ * 2 + 1 # windows diameter

        # w and h
        self.width_ = self.color_img_.shape[1]
        self.height_ = self.color_img_.shape[0]

        # output
        self.filt_prob_img_ = np.zeros((self.height_,self.width_), np.float32)

        # sigma parameters
        self.sigma_w_ = 0.2
        self.sigma_c_ = 10

    def filter(self):
        # loop over the image
        for y in range(0, self.height_):
            for x in range(0, self.width_):
                color_pix = self.color_img_[y,x].astype(int)
                prob_pix = self.prob_img_[y,x]

                iy = max(y - self.R_, 0)
                ix = max(x - self.R_, 0)
                tx = min(x + self.R_ + 1, self.width_ ) # min
                ty = min(y + self.R_ + 1, self.height_) # max

                sum1 = 0
                sum2 = 0
                color2 = 0
                space2 = 0

                for cy in range(iy, int(ty)):
                    for cx in range(ix, int(tx)):

                        if(cy==y and cx==x):
                            continue

                        color_temp = self.color_img_[cy,cx].astype(int)
                        prob_temp = self.prob_img_[cy, cx]

                        color2 = (color_temp[0] - color_pix[0])*(color_temp[0] - color_pix[0]) + (color_temp[1] - color_pix[1])*(color_temp[1] - color_pix[1]) + (color_temp[2] - color_pix[2])*(color_temp[2] - color_pix[2])
                        dist = np.exp(-int(color2) / (2 * self.sigma_c_ * self.sigma_c_))

                        space2 = (x - cx) * (x - cx) + (y - cy) * (y - cy)
                        spatial = np.exp(-space2 / (2 * self.sigma_w_ * self.sigma_w_))

                        weight = dist * spatial
                        sum1 += prob_temp * weight
                        sum2 += weight

                if( sum2 != 0 ):
                    res = (sum1 / sum2)
                else:
                    res = 0

                self.filt_prob_img_[y,x] = res

    @property
    def _filtered_image(self):
        return self.filt_prob_img_

def show_usage():
    print('[!] hello image filtering!')

if __name__ == "__main__":
    color_img = cv2.imread("D:\\Repo\\kaggle_segmentation\\input\\train\\1ba84b81628e_14.jpg")
    prob_img =  np.load("D:\\Repo\\kaggle_segmentation\\input\\data_4_eval\\1ba84b81628e_14.npy")

    #plt.imshow(prob_img)
    #plt.show()

    #plt.imshow(color_img)
    #plt.show()

    filtering_jbf = JointBilateralFiltering(color_img, prob_img, 2)
    filtering_jbf.filter()

    np.save("filtered.npy",filtering_jbf._filtered_image)

    plt.imshow(filtering_jbf._filtered_image)
    plt.show()

    # cv2.imshow("test",left_img)
    # cv2.waitKey(0)
    #rectifier.rectify_images(left_img, right_img)
    # show_usage()
    exit()