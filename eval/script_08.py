
import cv2
import numpy as np
import os
import matplotlib.pyplot as plt

import sys
import params

import pydensecrf.densecrf as dcrf
import matplotlib.pyplot as plt
from numpy import inf

from pydensecrf.utils import unary_from_labels, create_pairwise_bilateral, create_pairwise_gaussian

input_size = params.input_size
batch_size = params.batch_size
orig_width = params.orig_width
orig_height = params.orig_height
threshold = params.threshold
model = params.model

# visualise car error

# Get im{read,write} from somewhere.
try:
    from cv2 import imread, imwrite
except ImportError:
    # Note that, sadly, skimage unconditionally import scipy and matplotlib,
    # so you'll need them if you don't have OpenCV. But you probably have them.
    from skimage.io import imread, imsave
    imwrite = imsave
    # TODO: Use scipy instead.

def  refine_dense_crf(img, unary):
    n_labels = 2

    # -log prob
    U_f = -np.log(unary + np.finfo(np.float32).eps)
    U_b = -np.log(1 - unary + np.finfo(np.float32).eps)
    U_f[U_f == inf] = 0
    U_b[U_b == inf] = 0

    U_f = np.float32(U_f)
    U_b = np.float32(U_b)

    U = np.zeros((2, img.shape[0] * img.shape[1]), dtype=np.float32)
    U[0, :] = np.reshape(U_b, np.product(U_b.shape))
    U[1, :] = np.reshape(U_f, np.product(U_f.shape))

    # Example using the DenseCRF2D code
    d = dcrf.DenseCRF2D(img.shape[1], img.shape[0], n_labels)

    # get unary potentials (neg log probability)
    d.setUnaryEnergy(U)

    #U = U.reshape(img.shape[0], img.shape[1], n_labels)
    #plt.imshow(U[:,:,0])
    #plt.show()

    # This adds the color-independent term, features are the locations only.
    d.addPairwiseGaussian(sxy=(13, 13), compat=3, kernel=dcrf.FULL_KERNEL,
                          normalization=dcrf.NORMALIZE_SYMMETRIC)

    # This adds the color-dependent term, i.e. features are (x,y,r,g,b).
    d.addPairwiseBilateral(sxy=(260, 260), srgb=(15, 15, 15), rgbim=img,
                           compat=10 ,
                           kernel=dcrf.FULL_KERNEL,
                           normalization=dcrf.NORMALIZE_SYMMETRIC)

    # Run five inference steps.
    Q = d.inference(25)

    # Find out the most probable class for each pixel.
    MAP = np.argmax(Q, axis=0)
    MAP = MAP.reshape(img.shape[0], img.shape[1])

    return MAP

# draw -----------------------------------
def im_show(name, image, resize=1):
    H,W = image.shape[0:2]
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.imshow(name, image.astype(np.uint8))
    cv2.resizeWindow(name, round(resize*W), round(resize*H))


def draw_shadow_text(img, text, pt,  fontScale, color, thickness, color1=None, thickness1=None):
    if color1 is None: color1=(0,0,0)
    if thickness1 is None: thickness1 = thickness+2

    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(img, text, pt, font, fontScale, color1, thickness1, cv2.LINE_AA)
    cv2.putText(img, text, pt, font, fontScale, color,  thickness,  cv2.LINE_AA)

def draw_contour(image, mask, color=(0,255,0), thickness=1):
    threshold = 127
    ret, thresh = cv2.threshold(mask,threshold,255,0)
    ret = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    hierarchy = ret[0]
    contours  = ret[1]
    #image[...]=image
    cv2.drawContours(image, contours, -1, color, thickness, cv2.LINE_AA)
    ## drawContours(image, contours, contourIdx, color, thickness=None, lineType=None, hierarchy=None, maxLevel=None, offset=None): # real signature unknown; restored from __doc__


def draw_mask(img,  mask,  color=(0,255,0), alpha=1., beta=0.8):
    mask = np.dstack((mask,mask,mask))*np.array(color)
    mask = mask.astype(np.uint8)
    img[...] = cv2.addWeighted(img,  alpha, mask, beta,  0.) # image * α + mask * β + λ


#debug and show -----------------------------------------------------------------
def draw_contour_on_image(image, label, prob=None):
    results = image.copy()
    #if prob is not None:
    #    draw_contour(results, prob, color=(0,255,0), thickness=1)
    if label is not None:
        draw_contour(results, label, color=(0,0,255), thickness=1)

    return results


def draw_dice_on_image(image, label, prob, name=''):

    label = label>127
    prob = prob > 0.5
    score = one_dice_loss_py(label, prob)

    H,W,C = image.shape
    results = np.zeros((H*W,3),np.uint8)
    a = (2*label+prob).reshape(-1)
    miss = np.where(a==2)[0]
    hit  = np.where(a==3)[0]
    fp   = np.where(a==1)[0]
    label_sum = label.sum()
    prob_sum  = prob.sum()

    results[miss] = np.array([0,0,255])
    results[hit]  = np.array([64,64,64])
    results[fp]   = np.array([0,255,0])
    results = results.reshape(H,W,3)
    L=30
    draw_shadow_text  (results, '%s'%(name), (5,1*L),  1, (255,255,255), 2)
    draw_shadow_text  (results, '%0.5f'%(score), (5,2*L),  1, (255,255,255), 2)
    draw_shadow_text  (results, 'label = %0.0f'%(label_sum), (5,3*L),  1, (255,255,255), 2)
    draw_shadow_text  (results, 'prob = %0.0f (%0.4f)'%(prob_sum,prob_sum/label_sum), (5,4*L),  1, (255,255,255), 2)
    draw_shadow_text  (results, 'miss = %0.0f (%0.4f)'%(len(miss), len(miss)/label_sum), (5,5*L),  1, (0,0,255), 2)
    draw_shadow_text  (results, 'fp   = %0.0f (%0.4f)'%(len(fp), len(fp)/prob_sum), (5,6*L),  1, (0,255,0), 2)

    return results


def one_dice_loss_py(m1, m2):
    m1 = m1.reshape(-1)
    m2 = m2.reshape(-1)
    intersection = (m1 * m2)
    score = 2. * (intersection.sum()+1) / (m1.sum() + m2.sum()+1)
    return score

# main #################################################################
CARVANA_HEIGHT = 1280
CARVANA_WIDTH  = 1918

CARVANA_DIR = "E:\\Repo\\kaggle_segmentation\\kaggle_segmentation\\input\\"

model.load_weights(filepath='E:\\Repo\\kaggle_segmentation\\kaggle_segmentation\\weights\\best_weights_1280_sliding_window.hdf5')

if __name__ == '__main__':
    print( '%s: calling main function ... ' % os.path.basename(__file__))

    #example to make visualisation results
    name = '11fcda0a9e1c_12'
    #name = '2cb06c1f5bb1_14'
    #prob_file  = CARVANA_DIR + 'data_4_eval\\%s.npy'%name  #prediction from CNN
    label_file = CARVANA_DIR + 'train_masks\\%s_mask.png'%name
    img_file   = CARVANA_DIR + 'train\\%s.jpg'%name

    image = cv2.imread(img_file)
    label = cv2.imread(label_file,cv2.IMREAD_GRAYSCALE)
    #prob  = np.load(prob_file)
    #prob = cv2.medianBlur(prob, 5)

    x_batch = []
    print('../input/train/{}.jpg'.format(name))
    img = cv2.imread('../input/train/{}.jpg'.format(name))

    img1 = img[0:1280, 0:1280]
    img2 = img[0:1280, 638:1918]
    x_batch.append(img1)
    x_batch.append(img2)

    # img = cv2.resize(img, (input_size, input_size))
    # x_batch.append(img)
    x_batch = np.array(x_batch, np.float32) / 255
    preds = model.predict_on_batch(x_batch)
    preds = np.squeeze(preds, axis=3)

    prob = np.zeros([orig_height, orig_width])
    slice_counter = 0

    for pred in preds:
        # prob = cv2.resize(pred, (orig_width, orig_height))
        if (slice_counter == 0):
            prob[0:1280, 0:1280] = pred
        else:
            prob[0:1280, 638:1918] = pred
        slice_counter += 1

    #plt.imshow(prob)
    #plt.show()

    #prob = refine_dense_crf(img,prob)

    output_color_image = np.zeros((CARVANA_HEIGHT, CARVANA_WIDTH, 3), np.uint8)
    mask = prob > 0.5
    output_color_image[ mask == True ] = (255, 0, 0)
    output_color_image[ mask == False] = (0, 255, 0)
    cv2.imwrite('color.png', output_color_image)

    #prob = cv2.resize(prob,dsize=(CARVANA_WIDTH,CARVANA_HEIGHT))

    #threshold = 127
    #ret, thresh = cv2.threshold(label, threshold, 255, 0)
    #ret = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #hierarchy = ret[0]
    #contours = ret[1]
    #boundaries = np.zeros(( image.shape[0], image.shape[1] ))
    #cv2.drawContours(boundaries, contours, -1, (255,255,255), 1 , cv2.LINE_AA)
    #kernel = np.ones((5, 5), np.uint8)
    #boundaries = cv2.dilate(boundaries, kernel, iterations=1)
    #cv2.imshow("boundaries", boundaries)
    #cv2.waitKey(0)

    #draw hit,miss
    res = draw_dice_on_image(image, label, prob, name)
    cv2.imwrite('dice.png', res)
    im_show('dice', res, resize=0.33)
    cv2.waitKey(0)

    #draw contour
    #res = draw_contour_on_image(image, label, prob)
    #cv2.imwrite('contours.png', res)
    #im_show('contour', res, resize=0.33)
    #cv2.waitKey(0)
