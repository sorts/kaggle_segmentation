from model.u_net import get_unet_128, get_unet_256, get_unet_512, get_unet_1024, get_unet_1280_cropping, get_unet_1280_cropping_nfeatures, get_unet_224_vgg_weights

input_size = 1280

max_epochs = 500
batch_size = 2

orig_width = 1918
orig_height = 1280

threshold = 0.5

use_bbox = False
use_cropping = False

isolate_views = False

#BASE_DIR = 'D:\\Box Sync\\kaggle_segmentation\\'
BASE_DIR = "/mnt/md0/sorts/segmentation_kaggle"
#BASE_DIR = "E:\\Repo\\kaggle_segmentation\\kaggle_segmentation\\input"

model = get_unet_1280_cropping()