import cv2
import numpy as np
import pandas as pd
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint, TensorBoard
from sklearn.model_selection import train_test_split

import sys

from keras.applications.vgg16 import preprocess_input
import params

input_size = params.input_size
epochs = params.max_epochs
batch_size = params.batch_size
model = params.model
use_bbox = params.use_bbox
base_dir = params.BASE_DIR
use_cropping = params.use_cropping
isolate_views = params.isolate_views

viewn = 5

if(isolate_views):
    print('training model for viewn {}'.format(viewn))
    df_train = pd.read_csv('input/train_view{num:02d}.csv'.format(num=viewn))
else:
    df_train = pd.read_csv('input/train_masks.csv')

#
ids_train = df_train['img'].map(lambda s: s.split('.')[0])

ids_train_split, ids_valid_split = train_test_split(ids_train, test_size=0.2, random_state=12)

print('Training on {} samples'.format(len(ids_train_split)))
print('Validating on {} samples'.format(len(ids_valid_split)))

#mean_r = np.load("preprocess/mean_r.npy")
#mean_g = np.load("preprocess/mean_g.npy")
#mean_b = np.load("preprocess/mean_b.npy")

#std_r = np.load("preprocess/std_r.npy")
#std_g = np.load("preprocess/std_g.npy")
#std_b = np.load("preprocess/std_b.npy")

#ids_train_split = pd.Series()

# add DA
#for i in range(5000):
#    ids_train_split = ids_train_split.append( pd.Series([str(i+1)]) )

#print('(Data Augmentation) Training on {} samples'.format(len(ids_train_split)))

# dump valid names to a txt file
#file = open("validation.txt","w")
#for id in ids_valid_split.values:
#    file.write('{}\n'.format(id))
#file.close()

def randomShiftScaleRotate(image, mask,
                           shift_limit=(-0.0625, 0.0625),
                           scale_limit=(-0.1, 0.1),
                           rotate_limit=(-45, 45), aspect_limit=(0, 0),
                           borderMode=cv2.BORDER_CONSTANT, u=0.5):
    if np.random.random(): #< u:
        height, width, channel = image.shape

        angle = np.random.uniform(rotate_limit[0], rotate_limit[1])  # degree
        scale = np.random.uniform(1 + scale_limit[0], 1 + scale_limit[1])
        aspect = np.random.uniform(1 + aspect_limit[0], 1 + aspect_limit[1])
        sx = scale * aspect / (aspect ** 0.5)
        sy = scale / (aspect ** 0.5)
        dx = round(np.random.uniform(shift_limit[0], shift_limit[1]) * width)
        dy = round(np.random.uniform(shift_limit[0], shift_limit[1]) * height)

        cc = np.math.cos(angle / 180 * np.math.pi) * sx
        ss = np.math.sin(angle / 180 * np.math.pi) * sy
        rotate_matrix = np.array([[cc, -ss], [ss, cc]])

        box0 = np.array([[0, 0], [width, 0], [width, height], [0, height], ])
        box1 = box0 - np.array([width / 2, height / 2])
        box1 = np.dot(box1, rotate_matrix.T) + np.array([width / 2 + dx, height / 2 + dy])

        box0 = box0.astype(np.float32)
        box1 = box1.astype(np.float32)
        mat = cv2.getPerspectiveTransform(box0, box1)
        image = cv2.warpPerspective(image, mat, (width, height), flags=cv2.INTER_LINEAR, borderMode=borderMode,
                                    borderValue=(
                                        0, 0,
                                        0,))
        mask = cv2.warpPerspective(mask, mat, (width, height), flags=cv2.INTER_LINEAR, borderMode=borderMode,
                                   borderValue=(
                                       0, 0,
                                       0,))

    return image, mask


def randomHorizontalFlip(image, mask, u=0.5):
    if np.random.random() < u:
        image = cv2.flip(image, 1)
        mask = cv2.flip(mask, 1)

    return image, mask


def train_generator():
    while True:
        for start in range(0, len(ids_train_split), batch_size):
            x_batch = []
            y_batch = []
            end = min(start + batch_size, len(ids_train_split))
            ids_train_batch = ids_train_split[start:end]
            for id in ids_train_batch.values:

                if not id.isdigit():
                    #print(id)
                    img = cv2.imread('{}/train/{}.jpg'.format(base_dir,id))
                    mask = cv2.imread('{}/train_masks/{}_mask.png'.format(base_dir,id), cv2.IMREAD_GRAYSCALE)
                else:
                    img = cv2.imread('{}/train/{}.JPEG'.format(base_dir,id))
                    mask = cv2.imread('{}/train_masks/{}_mask.JPEG'.format(base_dir,id), cv2.IMREAD_GRAYSCALE)
                    mask[ mask > 200 ] = 255
                    mask[ mask <= 200 ] = 0

                if use_bbox == True:
                    # bbox training
                    bbox_file = open('input/train_bbox/{}.txt'.format(id), 'r')
                    temp_str = bbox_file.readline()
                    temp_list = temp_str.split()
                    xmin = int(float(temp_list[0]))
                    ymin = int(float(temp_list[1]))
                    xmax = int(float(temp_list[2]))
                    ymax = int(float(temp_list[3]))

                    # crop image
                    img = img[ymin:ymax, xmin:xmax]
                    mask = mask[ymin:ymax, xmin:xmax]

                if use_cropping == False:
                    #img preprocessing
                    img = np.float32(img)
                    img[:, :, 0] = (img[:, :, 0] - mean_b) / (std_b + 1e-8)
                    img[:, :, 1] = (img[:, :, 1] - mean_g) / (std_g + 1e-8)
                    img[:, :, 2] = (img[:, :, 2] - mean_r) / (std_r + 1e-8)

                    img = cv2.resize(img, (input_size, input_size))
                    mask = cv2.resize(mask, (input_size, input_size))

                    #img, mask = randomShiftScaleRotate(img, mask,
                    #                                     shift_limit=(-0.0625, 0.0625),
                    #                                     scale_limit=(-0.1, 0.1),
                    #                                     rotate_limit=(-0, 0))
                    #img, mask = randomHorizontalFlip(img, mask)
                    mask = np.expand_dims(mask, axis=2)
                    x_batch.append(img)
                    y_batch.append(mask)

                    #mask2 = np.expand_dims(mask2, axis=2)
                    #x_batch.append(img2)
                    #y_batch.append(mask2)
                else:
                    img1 = img[0:1280, 0:1280]
                    mask1 = mask[0:1280, 0:1280]

                    img1, mask1 = randomShiftScaleRotate(img1, mask1,
                                                   shift_limit=(-0.0625, 0.0625),
                                                   scale_limit=(-0.1, 0.1),
                                                   rotate_limit=(-0, 0))
                    img1, mask1 = randomHorizontalFlip(img1, mask1)

                    mask1 = np.expand_dims(mask1, axis=2)
                    x_batch.append(img1)
                    y_batch.append(mask1)

                    img2 = img[0:1280, 638:1918]
                    mask2 = mask[0:1280, 638:1918]

                    img2, mask2 = randomShiftScaleRotate(img2, mask2,
                                                     shift_limit=(-0.0625, 0.0625),
                                                     scale_limit=(-0.1, 0.1),
                                                     rotate_limit=(-0, 0))
                    img2, mask2 = randomHorizontalFlip(img2, mask2)

                    mask2 = np.expand_dims(mask2, axis=2)
                    x_batch.append(img2)
                    y_batch.append(mask2)

            y_batch = np.array(y_batch, np.float32) / 255

            #x_batch = np.array(x_batch, np.float32)
            #x_batch = preprocess_input(x_batch)
            x_batch = np.array(x_batch, np.float32) / 255

            yield x_batch, y_batch


def valid_generator():
    while True:
        for start in range(0, len(ids_valid_split), batch_size):
            x_batch = []
            y_batch = []
            end = min(start + batch_size, len(ids_valid_split))
            ids_valid_batch = ids_valid_split[start:end]
            for id in ids_valid_batch.values:

                img = cv2.imread('{}/train/{}.jpg'.format(base_dir,id))
                mask = cv2.imread('{}/train_masks/{}_mask.png'.format(base_dir,id), cv2.IMREAD_GRAYSCALE)

                if use_bbox == True:
                    # bbox
                    bbox_file = open('{}/train_bbox/{}.txt'.format(base_dir,id), 'r')
                    temp_str = bbox_file.readline();
                    temp_list = temp_str.split();
                    xmin = int(float(temp_list[0]))
                    ymin = int(float(temp_list[1]))
                    xmax = int(float(temp_list[2]))
                    ymax = int(float(temp_list[3]))

                    img = img[ymin:ymax, xmin:xmax]
                    mask = mask[ymin:ymax, xmin:xmax]

                if use_cropping == False:
                    img = cv2.resize(img, (input_size, input_size))
                    mask = cv2.resize(mask, (input_size, input_size))

                    mask = np.expand_dims(mask, axis=2)

                    x_batch.append(img)
                    y_batch.append(mask)
                else:
                    img1 = img[0:1280, 0:1280]
                    mask1 = mask[0:1280, 0:1280]
                    mask1 = np.expand_dims(mask1, axis=2)

                    x_batch.append(img1)
                    y_batch.append(mask1)

                    img2 = img[0:1280, 638:1918]
                    mask2 = mask[0:1280, 638:1918]
                    mask2= np.expand_dims(mask2, axis=2)

                    x_batch.append(img2)
                    y_batch.append(mask2)

            y_batch = np.array(y_batch, np.float32) / 255
            x_batch = np.array(x_batch, np.float32) / 255

            yield x_batch, y_batch


callbacks = [EarlyStopping(monitor='val_dice_loss',
                           patience=8,
                           verbose=1,
                           min_delta=1e-5,
                           mode='max'),
             ReduceLROnPlateau(monitor='val_dice_loss',
                               factor=0.1,
                               patience=4,
                               verbose=1,
                               epsilon=1e-4,
                               mode='max'),
             ModelCheckpoint(monitor='val_dice_loss',
                             filepath='weights/best_weights.hdf5',
                             save_best_only=True,
                             save_weights_only=True,
                             mode='max'),
             TensorBoard(log_dir='logs')]

model.fit_generator(generator=train_generator(),
                    steps_per_epoch=np.ceil(float(len(ids_train_split)) / float(batch_size)),
                    epochs=epochs,
                    verbose=2,
                    callbacks=callbacks,
                    validation_data=valid_generator(),
                    validation_steps=np.ceil(float(len(ids_valid_split)) / float(batch_size)))
