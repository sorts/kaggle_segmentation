import Augmentor

from Augmentor import Pipeline
p = Augmentor.Pipeline("D:\\box\\Box Sync\\kaggle_segmentation\\train\\") # ensure you press enter after this, don't just c&p this code.
Pipeline.set_seed(100)
p.flip_left_right(probability=0.8)
p.rotate(probability=0.4, max_left_rotation=8, max_right_rotation=8)
p.zoom(probability=0.3, min_factor=1.1, max_factor=1.4)
p.random_distortion(probability=0.5, grid_width=8, grid_height=8, magnitude=5)
p.sample(5000)

p = Augmentor.Pipeline("D:\\box\\Box Sync\\kaggle_segmentation\\train_masks\\") # ensure you press enter after this, don't just c&p this code.
Pipeline.set_seed(100)
p.flip_left_right(probability=0.8)
p.rotate(probability=0.4, max_left_rotation=8, max_right_rotation=8)
p.zoom(probability=0.3, min_factor=1.1, max_factor=1.4)
p.random_distortion(probability=0.5, grid_width=8, grid_height=8, magnitude=5)
p.sample(5000)